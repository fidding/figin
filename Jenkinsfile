pipeline {
    agent any
     environment {
        APP_NAME = 'figin'
        JENKINS_WORKSPACE = '/var/lib/jenkins/workspace'
        GOPATH = '/root/go'
    }
    stages {
        stage('Build') {
            steps {
                // 防止nohup进程被kill,在pipeline中设置JENKINS_NODE_COOKIE,在freestyle下设置BUILD_ID
                withEnv(['JENKINS_NODE_COOKIE=DONTKILLME']) {
                    sh """
                        ln -sf ${JENKINS_WORKSPACE}/figin-prod ${GOPATH}/src/gitlab.com/${APP_NAME}
                        cp config-example.yaml config.yaml
                       """
                    sh """
                        cd ${GOPATH}/src/gitlab.com/${APP_NAME}
                        go build main.go && pwd && go install
                       """
                    sh """
                        cd ${GOPATH}/src/gitlab.com/${APP_NAME}
                        make kill
                        nohup ${GOPATH}/bin/${APP_NAME} &
                       """
                }
            }
        }
        stage('Unit Test') {
            steps {
                sh """
                    cd ${GOPATH}/src/gitlab.com/${APP_NAME}
                    make test-report
                    echo "生成代码测试报告"
                    cat junit-report.xml
                    make test-covery
                    echo "生成代码覆盖率报告"
                    cat coverage.xml
                   """
            }
            post {
                always {
                     junit "junit-report.xml"
                     step([
                        $class: 'CoberturaPublisher',
                        autoUpdateHealth: false,
                        autoUpdateStability: false,
                        coberturaReportFile: 'coverage.xml',
                        failUnhealthy: false,
                        failUnstable: false,
                        maxNumberOfBuilds: 0,
                        onlyStable: false,
                        sourceEncoding: 'ASCII',
                        zoomCoverageChart: false
                     ])
                }
            }
        }
        stage('Deploy') {
            steps {
                 sh """
                     ps -ef | grep figin
                    """
            }
        }
    }
    post {
        always {
            echo 'This will always run'
        }
        success {
            echo 'This will run only if successful'
        }
        failure {
            echo 'This will run only if failed'
        }
        unstable {
            echo 'This will run only if the run was marked as unstable'
        }
        changed {
            echo 'This will run only if the state of the Pipeline has changed'
            echo 'For example, if the Pipeline was previously failing but is now successful'
        }
    }
}