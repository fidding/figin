#!/bin/bash

echo "mode: set" > cover.out
for Dir in $(go list ./...); 
do
    if [[ ${Dir} != *"/vendor/"* ]]
    then
        returnval=`go test -coverprofile=profile.out $Dir`
        echo ${returnval}
        if [[ ${returnval} != *FAIL* ]]
        then
            if [ -f profile.out ]
            then
                cat profile.out | grep -v "mode: set" >> cover.out
            fi
        else
            exit 1
        fi
    else
        exit 1
    fi  

done
if [ -n "$COVERALLS_TOKEN" ]
then
    /root/bin/goveralls -coverprofile=cover.out -repotoken=$COVERALLS_TOKEN -service=travis-pro
fi  