#!/bin/bash

go get -v github.com/jstemmer/go-junit-report
go test -v ./... | /root/go/bin/go-junit-report > junit-report.xml