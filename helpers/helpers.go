package helpers

import (
	"os"

	"github.com/satori/go.uuid"
)

// golang新版本的应该
func PathExist(_path string) bool {
	_, err := os.Stat(_path)
	if err != nil && os.IsNotExist(err) {
		return false
	}
	return true
}

// 生成uuid
func Uuid() string {
	uid := uuid.NewV4()
	return uid.String()
}

// 判断字符串数组里面是否有字符串值
func StrArrayIn(arr []string, val interface{}) (ok bool) {
	ok = false
	for _, item := range arr {
		if item == val {
			ok = true
			break
		}
	}
	return
}

