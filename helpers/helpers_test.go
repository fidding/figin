package helpers

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

type TestUuidCase struct {
	Case string
	Got  int
	NotWant int
}

// 测试uuid
func TestUuid(t *testing.T) {
	testCase := []TestUuidCase{
		{Case: "uuid长度不为0", Got: len(Uuid()), NotWant: 0},
	}
	testDesc := "测试Uuid生成"
	Convey(testDesc, t, func() {
		for _, item := range testCase {
			Convey(item.Case, func() {
				So(item.Got, ShouldNotEqual, item.NotWant)
			})
		}
	})
}

// 测试字符串数组是否存在某值
type TestStrArrayInCase struct {
	Case string
	Got  bool
	Want bool
}
func TestStrArrayIn(t *testing.T) {
	testArr := []string{"a", "b", "c"}
	testCase := []TestStrArrayInCase{
		{Case: "数组['a','b','c']存在字符串a", Got: StrArrayIn(testArr, "a"), Want: true},
		{Case: "数组['a','b','c']不存在字符串d", Got: StrArrayIn(testArr, "d"), Want: false},
	}
	testDesc := "测试字符串数组是否存在某值"
	Convey(testDesc, t, func() {
		for _, item := range testCase {
			Convey(item.Case, func() {
				So(item.Got, ShouldEqual, item.Want)
			})
		}
	})
}