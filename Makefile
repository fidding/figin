.PHONY: kill

ALL: kill,test-report,test-covery

kill:
	pkill -9 figin

test-report:
	./test-report.sh

test-covery:
	go get -v github.com/t-yuki/gocover-cobertura
	./test-covery.sh
	${GOPATH}/bin/gocover-cobertura < cover.out > coverage.xml