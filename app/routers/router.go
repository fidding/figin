package router

import (
	"github.com/gin-gonic/gin"

	"gitlab.com/figin/app/routers/api"
	"gitlab.com/figin/app/routers/web"
	"gitlab.com/figin/app/middlewares"
)

/**
 * 路由初始化
 */
func Init(router *gin.Engine) *gin.Engine {
	// 中间件
	router.Use(middleware.Logger())
	// 接口
	router.GET("/bus_pv", api.BusPv)
	router.Use(middleware.Auth())
	// HTML
	router.GET("/", web.Index)
	router.GET("/index", web.Index)
	
	return router
}